// Assignment 4 - Salary
// Corey Zimmerman

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Employee
{
	string firstName = "";
	string lastName = "";
	int ID = 0;
	double payRate = 0;
	double hoursWorked = 0;
};

double totalPay = 0; // stores the sum of all employee pay
void PrintEmployeeInfo(const Employee *pEmployee)
{	
	double grossPay = ((pEmployee->payRate) * (pEmployee->hoursWorked));
	
	cout << "#" << pEmployee->ID << " " << pEmployee->firstName << " " << pEmployee->lastName
		<< ": " << "Gross Pay: " << grossPay << "\n";
	totalPay += grossPay;
}

int main()
{
	int num = 0;
	const int NUM_OF_EMP = 5;
	Employee employees[NUM_OF_EMP];
	
	for (int i = 0; i < NUM_OF_EMP; i++)
	{
		cout << "Employee ID: ";
		cin >> employees[i].ID;

		while (!(employees[i].ID >> num))
		{
			cout << "Error: a number must be entered: \n";
			cin.clear();
			cin.ignore(25, '\n');
			cout << "Employee ID: ";
			cin >> employees[i].ID;
			break;
		}

		cout << "First Name: ";
		cin >> employees[i].firstName;

		cout << "Last Name: ";
		cin >> employees[i].lastName;

		cout << "Pay Rate: ";
		cin >> employees[i].payRate;

		while (!(employees[i].payRate != num))
		{
			cout << "Error: a number must be entered: \n";
			cin.clear();
			cin.ignore(30, '\n');
			cout << "Pay Rate: ";
			cin >> employees[i].payRate;
			break;
		}

		cout << "Hours Worked: ";
		cin >> employees[i].hoursWorked;

		while (!(employees[i].hoursWorked != num))
		{
			cout << "Error: a number must be entered: \n";
			cin.clear();
			cin.ignore(30, '\n');
			cout << "Hours Worked: ";
			cin >> employees[i].hoursWorked;
			break;
		}

		cout << "\n";
	}

	for (int i = 0; i < NUM_OF_EMP; i++)
	{
		PrintEmployeeInfo(&employees[i]);
	}

	cout << "Total Gross Pay for all Employees : " << totalPay;
	
	(void)_getch();
	return 0;
}
